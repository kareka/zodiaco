package clases;

public class Caballero {
   private String nombre;
   private String armadura;
   private String procedencia;    
   private int vida;
   private int cosmos;
   private Ataque[]ataque;

   
   public Caballero(String nombre, String armadura, String procedencia, int vida, int cosmos, Ataque[] ataque) {
	super();
	this.nombre = nombre;
	this.armadura = armadura;
	this.procedencia = procedencia;
	this.vida = vida;
	this.cosmos = cosmos;
	this.ataque = ataque;
}


public String getNombre() {
	return nombre;
}


public void setNombre(String nombre) {
	this.nombre = nombre;
}


public String getArmadura() {
	return armadura;
}


public void setArmadura(String armadura) {
	this.armadura = armadura;
}


public String getProcedencia() {
	return procedencia;
}


public void setProcedencia(String procedencia) {
	this.procedencia = procedencia;
}


public int getVida() {
	return vida;
}


public void setVida(int vida) {
	this.vida = vida;
}


public int getCosmos() {
	return cosmos;
}


public void setCosmos(int cosmos) {
	this.cosmos = cosmos;
}


public Ataque[] getAtaque() {
	return ataque;
}


public void setAtaque(Ataque[] ataque) {
	this.ataque = ataque;
}
   
   
   
}
