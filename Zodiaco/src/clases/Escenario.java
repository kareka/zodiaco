package clases;

public final class Escenario {
	private String nombre;
    private Jugador jugador;
    private Caballero[]caballeros;
	
    public Escenario(String nombre, Jugador jugador, Caballero[] caballeros) {
		super();
		this.nombre = nombre;
		this.jugador = jugador;
		this.caballeros = caballeros;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	public Caballero[] getCaballeros() {
		return caballeros;
	}

	public void setCaballeros(Caballero[] caballeros) {
		this.caballeros = caballeros;
	}
    
    
}
