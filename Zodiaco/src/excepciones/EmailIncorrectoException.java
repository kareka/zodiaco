package excepciones;

public class EmailIncorrectoException extends Exception{
	public EmailIncorrectoException(String msg) {
		super(msg);
	}
}
