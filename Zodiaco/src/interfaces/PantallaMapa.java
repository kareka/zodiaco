package interfaces;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;

public class PantallaMapa extends JPanel{
	private Ventana ventana;
	public PantallaMapa(Ventana v) {
		super();
		setBackground(new Color(153, 153, 255));
	  	setLayout(null);
	  		  
	/*En este bot�n podremos ir a la pantallla del Torneo Gal�ctico*/  	
	  	JButton botonTorneo = new JButton("");
	  	botonTorneo.setEnabled(false);
	  	botonTorneo.addMouseListener(new MouseAdapter() {
	  		@Override
	  		public void mouseClicked(MouseEvent arg0) {	 	  			
	  				v.irPantallaTorneo();	  			
	  		}
	  	});
	  	botonTorneo.setBounds(504, 0, 504, 268);
	  	add(botonTorneo);
	  	
	/*En este bot�n podremos ir a la pantalla del Santuario*/ 	
	  	JButton botonSantuario = new JButton("");
	  	botonSantuario.setEnabled(false);
	  	botonSantuario.addMouseListener(new MouseAdapter() {
	  		@Override
	  		public void mouseClicked(MouseEvent e) {
	  			v.irPantallaSantuario();
	  		}
	  	});
	  	botonSantuario.setBounds(0, 0, 504, 268);
	  	add(botonSantuario);
	  
   /*En este bot�n iremos a la pantalla de Asgard*/	  	
	  	JButton botonAsgard = new JButton("");
	  	botonAsgard.setEnabled(false);
	  	botonAsgard.addMouseListener(new MouseAdapter() {
	  		@Override
	  		public void mouseClicked(MouseEvent e) {
	  			v.irPantallaAsgard();
	  		}
	  	});
	  	botonAsgard.setBounds(0, 268, 504, 268);
	  	add(botonAsgard);
	  	
	/*En este bot�n iremos a la pantalla de Poseid�n*/  	
	  	JButton botonPoseidon = new JButton("");
	  	botonPoseidon.setEnabled(false);
	  	botonPoseidon.addMouseListener(new MouseAdapter() {
	  		@Override
	  		public void mouseClicked(MouseEvent e) {
	  			v.irPantallaPoseidon();
	  		}
	  	});
	  	botonPoseidon.setBounds(504, 268, 504, 268);	  	
	  	add(botonPoseidon);
	  	
	/*Co esto lo que hacemos es dejar los iconos desactivados hasta que no tengamos la puntuaci�n adecuada para  	
	 pelear en cada una de las pantallas del juego*/
		int puntos=v.getJugador().getPuntosAcumulados();
	  	switch(puntos) {
	  	case 0:
	  		botonTorneo.setEnabled(true);
	  	break;	
	  	case 150:
	  		botonSantuario.setEnabled(true);
	  	break;	
	  	case 300:
	  		botonAsgard.setEnabled(true);
	  	break;
	  	case 450:
	  		botonPoseidon.setEnabled(true);
	  	break;	
	  	
	  	}
	  	
	/*Esta etiqueta lo que hace es poner una imagen de fondo en nuestro mapa*/  	
	  	JLabel etiqueta_mapa_juego = new JLabel("");
	  	etiqueta_mapa_juego.setBounds(0, 0, 1008, 536);
	  	etiqueta_mapa_juego.setIcon(new ImageIcon("./imagenes/portada_elegir_pantalla_juego.png"));
	  	add(etiqueta_mapa_juego);
	  	
	
	  
	  	
	  	
	}
}
