package interfaces;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PantallaSantuario extends JPanel{
	private Ventana ventana;
    public PantallaSantuario (Ventana v) {
  	  super();
  	  setBackground(new Color(153, 153, 255));
  	  setLayout(null);
  
  	/*Esta es la segunda pantalla de combate de nuestro juego*/
  	 /*Asignamos una variable para la vida y el cosmos de nueatro caballero*/ 	
  	  int vidaJugador1=(v.getJugador().getCaballero().getVida())+20;
	  int cosmosJugador1=(v.getJugador().getCaballero().getCosmos())+20;
	
	  /*En este bot�n elegimos el tipo de ataque que menos da�o le har� a nuestro enemigo*/
  	  JButton botonGolpe = new JButton("GOLPE");
  	  botonGolpe.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
  	  botonGolpe.setBounds(88, 430, 185, 32);
  	  add(botonGolpe);
  	 
  	 /*En este bot�n elegimos el tipo de ataque medio*/
  	  JButton botonPoder = new JButton("PODER");
  	  botonPoder.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
  	  botonPoder.setBounds(329, 430, 185, 32);
  	  add(botonPoder);
  	  
  	/*En este bot�n elegiremos el ataque m�s letal que posee nuestro caballero*/	
  	  JButton botonFinal = new JButton("FINAL");
  	  botonFinal.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
  	  botonFinal.setBounds(574, 430, 185, 32);
  	  add(botonFinal);
  	  
  	/*En caso de perder el combate se nos dar� la oportunidad de continuar con la partida*/
  	  JLabel etiquetaContinuar = new JLabel("CONTINUAR");
  	  etiquetaContinuar.setForeground(Color.WHITE);
  	  etiquetaContinuar.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 20));
  	  etiquetaContinuar.setBounds(814, 347, 130, 32);
  	  add(etiquetaContinuar);
  	  
  	  JButton botonSi = new JButton("SI");
  	  botonSi.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
  	  botonSi.setBounds(836, 392, 88, 25);
  	  add(botonSi);
  	  
  	  JButton botonNo = new JButton("NO");
  	  botonNo.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 24));
  	  botonNo.setBounds(836, 435, 88, 25);
  	  add(botonNo);
  	  
  	/*Aqu� aparecer�n los puntos obtenidos por cada combate ganado, que se ir�n sumando al total de nuestro usuario*/	
  	  JTextPane textoPuntos = new JTextPane();
  	  textoPuntos.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 22));
  	  textoPuntos.setBounds(824, 41, 100, 30);
  	  String puntos=v.getJugador().getPuntosAcumulados()+" ptos";
	  textoPuntos.setText(puntos);
	  textoPuntos.getText();
  	  add(textoPuntos);
  	  
  	/*Esta barra de progreso est� unida a la variable de vida que creamos para nuestro jugador*/	
  	 JProgressBar vidaLuchador1 = new JProgressBar();
	  vidaLuchador1.setBackground(Color.WHITE);
	  vidaLuchador1.setString("");
	  vidaLuchador1.setStringPainted(true);
	  vidaLuchador1.setValue(vidaJugador1);
	  vidaLuchador1.setToolTipText("");
	  vidaLuchador1.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	  vidaLuchador1.setForeground(new Color(255, 0, 0));
	  vidaLuchador1.setBounds(340, 333, 146, 15);
	  add(vidaLuchador1);
	  
	  /*Esta barra de progreso est� unida a la variable de cosmos que creamos para nuestro jugador*/
	  JProgressBar cosmosLuchador1 = new JProgressBar();
	  cosmosLuchador1.setBackground(Color.BLACK);
	  cosmosLuchador1.setString("");
	  cosmosLuchador1.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	  cosmosLuchador1.setStringPainted(true);
	  cosmosLuchador1.setValue(cosmosJugador1);
	  cosmosLuchador1.setForeground(new Color(153, 255, 255));
	  cosmosLuchador1.setBounds(340, 360, 146, 15);
	  add(cosmosLuchador1);
	  
	  /*Esta barra ser� la de la vida de nuestro adversario*/
	  JProgressBar vidaLuchador2 = new JProgressBar();
	  vidaLuchador2.setBackground(Color.WHITE);
	  vidaLuchador2.setString("");
	  vidaLuchador2.setStringPainted(true);
	  vidaLuchador2.setValue(100);
	  vidaLuchador2.setToolTipText("");
	  vidaLuchador2.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));	  	  
	  vidaLuchador2.setForeground(new Color(255, 0, 0));
	  vidaLuchador2.setBounds(599, 333, 146, 15);
	  add(vidaLuchador2);
	  
	  /*Esta barra ser� la del cosmos de nuestro adversario*/
	  JProgressBar cosmosLuchador2 = new JProgressBar();
	  cosmosLuchador2.setStringPainted(true);
	  cosmosLuchador2.setBackground(Color.BLACK);
	  cosmosLuchador2.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	  cosmosLuchador2.setForeground(new Color(153, 255, 255));
	  cosmosLuchador2.setString("");
	  cosmosLuchador2.setValue(100);
	  cosmosLuchador2.setBounds(599, 360, 146, 15);
	  add(cosmosLuchador2);
	  
	  /*Estos dos botones nos sirven para poner las etiquetas de las imagenes de nuestros luchadores*/
	  JButton luchador1 = new JButton("");
	  luchador1.setOpaque(false);
	  luchador1.setEnabled(false);
	  luchador1.setBounds(364, 87, 100, 218);
	  add(luchador1);
	  
	  JButton luchador2 = new JButton("");
	  luchador2.setOpaque(false);
	  luchador2.setEnabled(false);
	  luchador2.setBounds(621, 87, 100, 218);
	  add(luchador2);
	  
	  JLabel imagenLuchador1 = new JLabel("New label");
	  imagenLuchador1.setBounds(364, 87, 100, 218);
	  imagenLuchador1.setIcon(new ImageIcon("./imagenes/Seiya.jpg"));
	  add(imagenLuchador1);
	  
	  JLabel imagenLuchador2 = new JLabel("New label");
	  imagenLuchador2.setBounds(621, 87, 100, 218);
	  imagenLuchador2.setIcon(new ImageIcon("./imagenes/leo2.jpg"));
	  add(imagenLuchador2);
  	  
	  /*Este panel nos mostrar� el desarrollo de la lucha ataque a ataque*/	
	  JTextPane panelJuego = new JTextPane();
	  panelJuego.setBounds(90, 87, 175, 292);	  
	  add(panelJuego);
	  
	  /*Con este bot�n volveremos al mapa para poder pasar a otra pantalla*/
	  JButton botonAtras = new JButton("ATRAS");
	  botonAtras.addMouseListener(new MouseAdapter() {
	  	@Override
	  	public void mouseClicked(MouseEvent e) {
	  		v.irPantallaMapa();
	  	}
	  });
	  botonAtras.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 18));
	  botonAtras.setBounds(50, 25, 136, 25);
	  add(botonAtras);
	  
	  /*Estas dos etiquetas nos sirven para mostrar a los usuarios cu�l es la barra de vida y cu�l la de cosmos*/
	  JLabel etiquetaVida = new JLabel("VIDA");
	  etiquetaVida.setHorizontalAlignment(SwingConstants.CENTER);
	  etiquetaVida.setForeground(Color.WHITE);
	  etiquetaVida.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	  etiquetaVida.setBounds(510, 330, 56, 16);
	  add(etiquetaVida);
	  
	  JLabel lblCosmos = new JLabel("COSMOS");
	  lblCosmos.setForeground(Color.WHITE);
	  lblCosmos.setHorizontalAlignment(SwingConstants.CENTER);
	  lblCosmos.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 13));
	  lblCosmos.setBounds(498, 357, 78, 16);
	  add(lblCosmos);
	  
	  /*Esta etiqueta nos sirve para poner una imagen de fondo a nuestra pantalla de juego*/
	  JLabel etiqueta_fondo_santuario = new JLabel("New label");
	  etiqueta_fondo_santuario.setBounds(0,0,1008,536);
	  etiqueta_fondo_santuario.setIcon(new ImageIcon("./imagenes/santuario.jpg"));
	  add(etiqueta_fondo_santuario);
	  
	
	  
	  
	  
	 
	  
	 
	  
	 
    }
}
