package interfaces;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

import clases.CaballeroBronce;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PantallaCargarPartida extends JPanel{
	public PantallaCargarPartida (Ventana v) {
	  	  super();
	  	  setBackground(new Color(153, 153, 255));
	  	  setLayout(null);
	  	  
	  	  JLabel etiquetaOpciones = new JLabel("OPCIONES");
	  	  etiquetaOpciones.setHorizontalAlignment(SwingConstants.CENTER);
	  	  etiquetaOpciones.setForeground(Color.WHITE);
	  	  etiquetaOpciones.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 34));
	  	  etiquetaOpciones.setBounds(393, 37, 220, 50);
	  	  add(etiquetaOpciones);
	  	  
	/*Este evento nos sirve para ir a la pantalla mapa pero cargando los datos de la ultima partida de 
	 nuestro usuario*/  	  
	  	  JButton botonCargarPartida = new JButton("CARGAR PARTIDA");
	  	  botonCargarPartida.addMouseListener(new MouseAdapter() {
	  	  	@Override
	  	  	public void mouseClicked(MouseEvent arg0) {
	  	  		
	  	  	        
	  	  	}
	  	  });
	  	  botonCargarPartida.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
	  	  botonCargarPartida.setBounds(345, 196, 320, 40);
	  	  add(botonCargarPartida);
	  	  
	 /*Este evento hace que se elimine el registro de caballero_nombre de nuestra bbdd, y posteriormente nos mandar�
	   a la pantalla de elegir personaje*/ 	  
	  	  JButton botonNuevaPartida = new JButton("NUEVA PARTIDA");
	  	  botonNuevaPartida.addMouseListener(new MouseAdapter() {
	  	  	@Override
	  	  	public void mouseClicked(MouseEvent e) {	  	  	
	    	  	v.eliminaCaballero();
	    	  	v.irPantallaElegirPersonaje();
	  	  	}
	  	  });
	  	  botonNuevaPartida.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 28));
	  	  botonNuevaPartida.setBounds(345, 319, 320, 40);
	  	  add(botonNuevaPartida);
	 /*Esta etiqueta sirve para establecer un fondo con una imagen para la pantalla cargar partida*/ 	  
	  	  JLabel fondoCargarPartida = new JLabel("");
	  	  fondoCargarPartida.setBounds(0, 0, 1008, 536);
	  	  fondoCargarPartida.setIcon(new ImageIcon("./imagenes/continuar_partida.jpg"));
	  	  add(fondoCargarPartida);
	}
}
