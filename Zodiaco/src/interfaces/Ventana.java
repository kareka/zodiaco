package interfaces;

import java.awt.Image;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;

import interfaces.PantallaLogin;
import interfaces.PantallaRegistro;
import interfaces.PantallaInicio;
import clases.Ataque;
import clases.Caballero;
import clases.CaballeroBronce;
import clases.CaballeroHilda;
import clases.CaballeroOro;
import clases.CaballeroPoseidon;
import clases.Escenario;
import clases.Jugador;

public class Ventana extends JFrame{
   private PantallaInicio pantallaInicio;
   private PantallaRegistro pantallaRegistro;
   private PantallaLogin pantallaLogin;
   private PantallaModoJuego pantallaModoJuego;
   private PantallaElegirPersonaje pantallaElegirPersonaje;
   private PantallaElegirEquipo pantallaElegirEquipo;
   private PantallaTorneo pantallaTorneo;
   private PantallaSantuario pantallaSantuario;   
   private PantallaIntroduccion pantallaIntroduccion;
   private PantallaAsgard pantallaAsgard;
   private PantallaPoseidon pantallaPoseidon;
   private PantallaMapa pantallaMapa;
   private PantallaCargarPartida pantallaCargarPartida;
   private Jugador jugador;
   public Connection conexion;
   
   public Ventana() {	   
	   super();
	   this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	   pantallaInicio= new PantallaInicio(this);
	   this.setTitle("PantallaInicio");
	   setSize(1008,536);
	   setVisible(true);
	   this.setContentPane(pantallaInicio);
  /*creacion de conexi�n a mi base de datos, con comprobaci�n de conexi�n*/
	   conexion=null;
	   try {
		setConexion(DriverManager.getConnection("jdbc:mysql://localhost:3306/cabzodiaco","root","1234"));
		System.out.println("Estoy conectado");
	   } catch (SQLException e) {
		// TODO Auto-generated catch block
		   System.err.println("La conexi�n a base de datos ha fallado");
		e.printStackTrace();
	   }
         
	   
       }
   

   public Connection getConexion() {
	return conexion;
   }


   public void setConexion(Connection conexion) {
	this.conexion = conexion;
   }


   public Jugador getJugador() {
	return jugador;
   }

   public void setJugador(Jugador jugador) {
	this.jugador = jugador;
   }
/*Esta funcion debe de poner el registro caballero_nombre a null, para poder empezar una partida nueva con otro caballero*/   
   public void eliminaCaballero() {
	   PreparedStatement smt=null;
       String nombre=this.getJugador().getUsuario();
  	  	try {
			smt=this.getConexion().prepareStatement("update cabzodiaco.jugador set caballero_nombre=null  where usuario=?;");			
			smt.setString(1,nombre);
			smt.executeUpdate();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
   }
   /*Esta funci�n nos servir� para asignar a nuestro jugador un caballero por medio de un evento de rat�n, al clickar en una 
     de las imagenes de nuestros caballeros se lo asignaremos en la base de datos y posteriormente en el c�digo java.
     La variable nombreCaballero que se le pasa por par�metros es el nombre que insertamos por medio del evento*/
   public void asignaCaballero(String nombreCaballero) {	
	     switch(nombreCaballero) {
	     case "Shiryu":
	    	 this.getJugador().setCaballero(caballeroBronceDragon);
	     break;
	     case "Seiya":
	    	 this.getJugador().setCaballero(caballeroBroncePegaso);
	     break;
	     case "Yoga":
	    	 this.getJugador().setCaballero(caballeroBronceCisne);
	     break;
	     case "Shun":
	    	 this.getJugador().setCaballero(caballeroBronceAndromeda);
	     break;
	     case "Ikki":
	    	 this.getJugador().setCaballero(caballeroBronceFenix);
	     break; 	 
	     }
	 	 PreparedStatement smt=null;
         String nombre=this.getJugador().getUsuario();
    	  	try {
 			smt=this.getConexion().prepareStatement("update cabzodiaco.jugador set caballero_nombre=?  where usuario=?;");
 			smt.setString(1,nombreCaballero);
 			smt.setString(2,nombre);
 			smt.executeUpdate();
 		} catch (SQLException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		}
   }
   /*Esta funci�n nos va a servir para desplazarnos a la pantalla de login de nuestro juego*/
   public void irPantallaLogin(){
	   if(this.pantallaLogin==null) {
		   this.pantallaLogin=new PantallaLogin(this);
	   }
	   this.setTitle("pantallaLogin");
	   this.pantallaInicio.setVisible(false);
	   this.setContentPane(this.pantallaLogin);
	   this.pantallaLogin.setVisible(true);
	   
   }

 /*Esta funci�n nos va a servir para desplazarnos a la pantalla de registro de nuestro juego*/
   public void irPantallaRegistro() {
	   if(this.pantallaRegistro==null) {
		   this.pantallaRegistro=new PantallaRegistro(this);
	   }
	   this.setTitle("pantallaRegistro");
	   this.pantallaInicio.setVisible(false);
	   this.setContentPane(this.pantallaRegistro);
	   this.pantallaRegistro.setVisible(true);
   }

 /*Esta funci�n nos va a servir para volver a la pantalla de inicio de nuestro juego, tanto si estamos
   en el registro como si estamos en el login*/
   public void irPantallaInicio() {
	   
	   if(this.pantallaLogin!=null) {
		   this.pantallaLogin.setVisible(false);
	   }
	   if(this.pantallaRegistro!=null) {
		   this.pantallaRegistro.setVisible(false);
	   }
	   this.setTitle("pantallaInicio");
	   this.setContentPane(this.pantallaInicio);
	   this.pantallaInicio.setVisible(true);
   }
 /*Esta funci�n nos va a servir para desplazarnos a la pantalla de modos de juego desde registro */   
   public void irPantallaModoJuego() {
	   if(this.pantallaModoJuego==null) {
		   this.pantallaModoJuego=new PantallaModoJuego(this);
	   }
	   this.setTitle("PantallaModoJuego");
	   if(this.pantallaLogin!=null) {
		   this.pantallaLogin.setVisible(false);
	   }
	   if(this.pantallaRegistro!=null) {
		   this.pantallaRegistro.setVisible(false);
	   }
	   this.setContentPane(this.pantallaModoJuego);
	   this.pantallaModoJuego.setVisible(true);
   }
  /*Esta funci�n nos servir� para poder desplazarnos a la pantalla de elecci�n de nuestro
   personaje.*/ 
   public void irPantallaElegirPersonaje() {
	   if(this.pantallaElegirPersonaje==null) {
		   this.pantallaElegirPersonaje=new PantallaElegirPersonaje(this);
	   }
	   this.setTitle("PantallaElegirPersonaje");
	   if(this.pantallaCargarPartida!=null) {
		   this.pantallaCargarPartida.setVisible(false);		   
	   }
	   if(this.pantallaModoJuego!=null) {
		   this.pantallaModoJuego.setVisible(false);
	   }
	   this.setContentPane(this.pantallaElegirPersonaje);
	   this.pantallaElegirPersonaje.setVisible(true);
   }
   /*Esta funci�n nos va a permitir dirigirnos a una pantalla en la cual elegiremos seguir con nuestra partida
    o empezar una nueva, desde la pantalla de login*/
   public void irPantallaCargarPartida() {
	   if(this.pantallaCargarPartida==null) {
		   this.pantallaCargarPartida=new PantallaCargarPartida(this);
	   }
	   this.setTitle("PantallaCargarPartida");
	   this.pantallaLogin.setVisible(false);
	   this.setContentPane(this.pantallaCargarPartida);
	   this.pantallaCargarPartida.setVisible(true);
   }
   /*Esta funcion nos llevar� a la pantalla de introducci�n del juego, desde la pantalla elegir personaje*/
   public void irPantallaIntroduccion() {
	   if(this.pantallaIntroduccion==null) {
		   this.pantallaIntroduccion=new PantallaIntroduccion(this);
	   }
	   this.setTitle("PantallaIntroduccion");
	   this.pantallaElegirPersonaje.setVisible(false);
	   this.setContentPane(this.pantallaIntroduccion);
	   this.pantallaIntroduccion.setVisible(true);
   }
   /*Esta funci�n nos permite llegar hasta el mapa del juego desde la pantalla de introduccion o la pantalla de cargar partida*/
   public void irPantallaMapa() {
	   if(this.pantallaMapa==null) {
		   this.pantallaMapa=new PantallaMapa(this);		   
	   }
	   this.setTitle("PantallaMapa");
	   if(this.pantallaCargarPartida!=null) {
		   this.pantallaCargarPartida.setVisible(false);
	   }	   	   
	   if(this.pantallaIntroduccion!=null) {
		   this.pantallaIntroduccion.setVisible(false);
	   }
	   this.setContentPane(this.pantallaMapa);
	   this.pantallaMapa.setVisible(true);
   }
   /*Esta funci�n nos llevar� a la primera pantalla de combate de nuestro juego, desde la pantalla mapa*/
   public void irPantallaTorneo() {
	   if(this.pantallaTorneo==null) {
		   this.pantallaTorneo=new PantallaTorneo(this);
	   }
	   this.setTitle("PantallaTorneo");
	   this.pantallaMapa.setVisible(false);
	   this.setContentPane(this.pantallaTorneo);
	   this.pantallaTorneo.setVisible(true);
   }
   /*Esta funci�n nos llevar� a la segunda pantalla de combate de nuestro juego, desde la pantalla mapa*/
   public void irPantallaSantuario() {
	   if(this.pantallaSantuario==null) {
		   this.pantallaSantuario=new PantallaSantuario(this);
	   }
	   this.setTitle("PantallaSantuario");
	   this.pantallaMapa.setVisible(false);
	   this.setContentPane(this.pantallaSantuario);
	   this.pantallaSantuario.setVisible(true);
   }
   /*Esta funci�n nos llevar� a la tercera pantalla de combate de nuestro juego, desde la pantalla mapa*/
   public void irPantallaAsgard() {
	   if(this.pantallaAsgard==null) {
		   this.pantallaAsgard=new PantallaAsgard(this);
	   }
	   this.setTitle("PantallaAsgard");
	   this.pantallaMapa.setVisible(false);
	   this.setContentPane(this.pantallaAsgard);
	   this.pantallaAsgard.setVisible(true);
   }
   
   /*Esta funci�n nos llevar� a la cuarta y �ltima pantalla de combate de nuestro juego, desde la pantalla mapa*/
   public void irPantallaPoseidon() {
	   if(this.pantallaPoseidon==null) {
		   this.pantallaPoseidon=new PantallaPoseidon(this);
	   }
	   this.setTitle("PantallaPoseidon");
	   this.pantallaMapa.setVisible(false);
	   this.setContentPane(this.pantallaPoseidon);
	   this.pantallaPoseidon.setVisible(true);
   }
  /*Esta funci�n nos permitir� elegir un rival de forma aleatoria dentro de un array de caballeros existente en cada escenario*/ 
   public static  Caballero  eligeRival(Escenario escenario,CaballeroBronce jugador1){//arreglar el fallo con un do..while                
       int numero ;              
       Caballero rival=null;
       do{
         numero= (int) (Math.random()*(escenario.getCaballeros().length-1)+0);  
       }while(!((escenario.getCaballeros()[numero].getVida()>0)&&(escenario.getCaballeros()[numero]!=jugador1)));
            
                  rival=escenario.getCaballeros()[numero];                                   
          
                
                          
             return rival;
            
  }
 
   /*Con esta funci�n controlaremos los ataques de nuestro caballero*/
   public static void ataque(Caballero jugador1,Caballero jugador2,int tipoAtaque,JTextPane panelJuego){
       
       
       switch(tipoAtaque){
           case 1 :
            if(jugador1.getVida()!=0){   
               panelJuego.setText("Ataca"+" "+jugador1.getNombre());
               panelJuego.getText();
               if(jugador1.getCosmos()>=20){
               panelJuego.setText(jugador1.getAtaque()[0].getNombre());
               panelJuego.getText();
               jugador1.setCosmos(jugador1.getCosmos()-20);
                  //Vamos a generar un numero aleatorio para saber si el ataque ha sido efectivo o no
               int numero = (int) (Math.random()*2+1);
                if(numero==1){ 
                    panelJuego.setText("El ataque ha tenido exito");
                    panelJuego.getText();
                   
                    if(jugador2.getVida()>20){
                    jugador2.setVida(jugador2.getVida()-20);
                    }else{
                        jugador2.setVida(0);
                    }
                }else{
                    panelJuego.setText("El ataque ha fallado");
                    panelJuego.getText();
                 
                    jugador2.setVida(jugador2.getVida());
                }
               }else{
                   panelJuego.setText("No tiene suficiente cosmos para realizar un ataque.");
                   panelJuego.getText();
                 
                   int recargaCosmos=100-jugador1.getCosmos();
                   jugador1.setCosmos(jugador1.getCosmos()+recargaCosmos);
                }
            
            }  
             
               
           break;
           case 2:
            if(jugador1.getVida()!=0){   
               if(jugador1.getCosmos()>=30){
               panelJuego.setText(jugador1.getAtaque()[1].getNombre());
               panelJuego.getText();
                   jugador1.setCosmos(jugador1.getCosmos()-30);
                             int numero = (int) (Math.random()*2+1);
                if(numero==1){ 
                    panelJuego.setText("El ataque ha tenido exito");
                    panelJuego.getText();
                   
                     if(jugador2.getVida()>30){
                    jugador2.setVida(jugador2.getVida()-30);
                    }else{
                        jugador2.setVida(0);
                    }
                }else{
                    panelJuego.setText("El ataque ha fallado");
                    panelJuego.getText();
                     
                    jugador2.setVida(jugador2.getVida());
                }
               }else{
                   panelJuego.setText("No tiene suficiente cosmos para realizar un ataque");
                   panelJuego.getText();
                  
                     int recargaCosmos=100-jugador1.getCosmos();
                    jugador1.setCosmos(jugador1.getCosmos()+recargaCosmos);
               }
            }
                  
           break;
           case 3:
            if(jugador1.getVida()!=0){     
               if(jugador1.getCosmos()>=50){
                  panelJuego.setText(jugador1.getAtaque()[2].getNombre());
                  panelJuego.getText();
               
                  jugador1.setCosmos(jugador1.getCosmos()-50);
                   int numero = (int) (Math.random()*2+1);
                if(numero==1){ 
                    panelJuego.setText("El ataque ha tenido exito");
                    panelJuego.getText();
                     
                 if(jugador2.getVida()>50){
                   jugador2.setVida(jugador2.getVida()-50);
                    }else{
                        jugador2.setVida(0);
                    }
                }else{
                    panelJuego.setText("El ataque ha fallado");
                    panelJuego.getText();
                     
                    jugador2.setVida(jugador2.getVida());
                }
               }else{
                   panelJuego.setText("No tiene suficiente cosmos para realizar un ataque");
                   panelJuego.getText();
                  
                      int recargaCosmos=100-jugador1.getCosmos();
                     jugador1.setCosmos(jugador1.getCosmos()+recargaCosmos);
               }
            }    
                 
           break;    
            default:
               panelJuego.setText("No es una opci�n v�lida. Vuelve a introducir");
               panelJuego.getText();
          break;
            
       
 }

       
    
   
}

 //Esta funci�n nos da el resultado de enfrentarnos a cada adversario de los distintos escenarios
 //He incluido una opci�n de continuar la lucha si el rival nos gana, para que el juego no se acabe tan pronto.
 //Tambi�n nos va a proporcionar los puntos obtenidos en cada enfrentamiento.    
     public static int resultadoLucha(Caballero jugador1,Caballero jugador2,JTextPane panelJuego ){
         String continuar;
         int puntos=0;
        
         while ((jugador1.getVida()>0)&&(jugador2.getVida()>0)){
             //Procedemos a introducir el ataque de nuestro jugador 
     
                 
                 panelJuego.setText("Elige un tipo de ataque :\t\n1-Golpe\t\n2-Poder\t\n3-Final ");
                 panelJuego.getText();
                   int tipoAtaque;              
                  ataque(jugador1,jugador2,tipoAtaque,panelJuego);
                  
                  panelJuego.setText("_______________________________________________________________________");
                  panelJuego.getText();
                    
                  
              //Aqui obtendremos el ataque de nuestro adversario      
                    int tipoAtaque2 = (int) (Math.random()*3+1);                   
                    ataque(jugador2,jugador1,tipoAtaque2,panelJuego);
                   
                    panelJuego.setText("_______________________________________________________________________");                 
                    panelJuego.getText();
                    if(jugador1.getVida()==0){
                        panelJuego.setText("\nEl caballero "+" " +jugador2.getNombre()+" "+"ha ganado la pelea");  
                        
                        panelJuego.setText("Deseas contibuar S/N:");
                        panelJuego.getText();
                        continuar=sc.nextLine();
                        if(continuar.equalsIgnoreCase("s")){                      
                                                       jugador1.setVida(100);
                                                       jugador2.setVida(100);
                                                       jugador1.setCosmos(100);
                                                       jugador2.setCosmos(100);
                        }else{
                            panelJuego.setText("Juego terminado");
                            panelJuego.getText();
                            System.exit(0);
                            
                        }
                   }
                 if(jugador2.getVida()==0){
                      panelJuego.setText("\nEl caballero "+" " +jugador1.getNombre()+" "+"ha ganado la pelea");    
                      panelJuego.getText();
                                                      
                                                       puntos=puntos+50;
                                              
                 }
              
             
        }
         return puntos;
     }   
   /*Con esta funci�n controlamos las tres luchas que hacen falta para pasar de pantalla*/  
     public static int faseJuego(Escenario escenario, CaballeroBronce  jugador1, JTextPane panelJuego, int puntosAcumulados){
         int contador=0;
         int pTotales=puntosAcumulados;
                              
                                       do{                                               
                                        Caballero jugador2=eligeRival(escenario, jugador1);
                                        int puntos=resultadoLucha(jugador1, jugador2, panelJuego);
                                          pTotales=pTotales+puntos;                                                                                   
                                         jugador1.setVida(100);
                                         jugador1.setCosmos(100);
                                        contador++;      
                                       
                                       }while(contador<4);                                        
                                       return pTotales;
                                     
    
     }

   
 /*A partir de aqu� empieza la declaraci�n de todos los objetos que son necesarios para la correcta ejecuci�n del programa*/  
 //Ataques de los caballeros de bronce
   //Ataques del caballero del Dragon
   Ataque ataqueDragon1 = new Ataque("Excalibur", "golpe", 20, 20);
   Ataque ataqueDragon2 = new Ataque("Vuelo del dragon", "poder", 30, 30);
   Ataque ataqueDragon3 = new Ataque("Elevacion del dragon", "final", 50, 50);
   //Ataques del caballero de Andromeda
   Ataque ataqueAndromeda1 = new Ataque("Nebulosa de andromeda", "golpe", 20, 20);
   Ataque ataqueAndromeda2 = new Ataque("Cadena nebular", "poder", 30, 30);
   Ataque ataqueAndromeda3 = new Ataque("Defensa giratoria", "final", 50, 50);
   //Ataques del caballero del Cisne
   Ataque ataqueCisne1 = new Ataque("Pu�o de hielo", "golpe", 20, 20);
   Ataque ataqueCisne2 = new Ataque("Rayo de la aurora", "poder", 30, 30);
   Ataque ataqueCisne3 = new Ataque("Polvo de diamante", "final", 50, 50);
   //Ataques del caballero del Fenix
   Ataque ataqueFenix1 = new Ataque("Pu�o fantasma", "golpe", 20, 20);
   Ataque ataqueFenix2 = new Ataque("Alas ardientes", "poder", 30, 30);
   Ataque ataqueFenix3 = new Ataque("Ilusion diabolica", "final", 50, 50);
   //Ataques del caballero del Pegaso
   Ataque ataquePegaso1 = new Ataque("Meteoro de pegaso", "golpe", 20, 20);
   Ataque ataquePegaso2 = new Ataque("Choque giratorio", "poder", 30, 30);
   Ataque ataquePegaso3 = new Ataque("Cometa de pegaso", "final", 50, 50);
   
   //Ataques de los caballeros de oro
   //Ataques del caballero de Leo
   Ataque ataqueLeo1 = new Ataque("Pu�o de leo", "golpe", 20, 20);
   Ataque ataqueLeo2 = new Ataque("Kens de energia", "poder", 30, 30);
   Ataque ataqueLeo3 = new Ataque("Energia dorada", "final", 50, 50);
   //Ataques del caballero de Geminis
   Ataque ataqueGeminis1 = new Ataque("Pu�o de luz", "golpe", 20, 20);
   Ataque ataqueGeminis2 = new Ataque("Cosmos fulminante", "poder", 30, 30);
   Ataque ataqueGeminis3 = new Ataque("Explosion de galaxias", "final", 50, 50);
   //Ataques del caballero de Tauro
   Ataque ataqueTauro1 = new Ataque("Pu�o de hierro", "golpe", 20, 20);
   Ataque ataqueTauro2 = new Ataque("Embestida del toro", "poder", 30, 30);
   Ataque ataqueTauro3 = new Ataque("Gran cuerno", "final", 50, 50);
   //Ataques del caballero de Aries
   Ataque ataqueAries1 = new Ataque("Muro de cristal", "golpe", 20, 20);
   Ataque ataqueAries2 = new Ataque("Red de cristal", "poder", 30, 30);
   Ataque ataqueAries3 = new Ataque("Espiral", "final", 50, 50);
   //Ataques del caballero de Escorpio
   Ataque ataqueEscorpio1 = new Ataque("Aguja escarlata", "golpe", 20, 20);
   Ataque ataqueEscorpio2 = new Ataque("Aguja de fuego", "poder", 30, 30);
   Ataque ataqueEscorpio3 = new Ataque("Esfera de energia", "final", 50, 50);
   
   //Ataques del caballero de Zeta
   Ataque ataqueZeta1 = new Ataque("Garra de tigre vikingo", "golpe", 20, 20);
   Ataque ataqueZeta2 = new Ataque("Sombra del tigre", "poder", 30, 30);
   Ataque ataqueZeta3 = new Ataque("Impulso azul", "final", 50, 50);
   //Ataques del caballero de Epsilon
   Ataque ataqueEpsilon1 = new Ataque("Garra feroz", "golpe", 20, 20);
   Ataque ataqueEpsilon2 = new Ataque("Lobo de la estepa", "poder", 30, 30);
   Ataque ataqueEpsilon3 = new Ataque("Ataque de la jauria", "final", 50, 50);
   //Ataques del caballero de Beta
   Ataque ataqueBeta1 = new Ataque("Rayo de fuego", "golpe", 20, 20);
   Ataque ataqueBeta2 = new Ataque("Congelacion del universo", "poder", 30, 30);
   Ataque ataqueBeta3 = new Ataque("Gran presion ardiente", "final", 50, 50);
   //Ataques del caballero de Gamma
   Ataque ataqueGamma1 = new Ataque("Martillo de Mjolnir", "golpe", 20, 20);
   Ataque ataqueGamma2 = new Ataque("Heracles titanico", "poder", 30, 30);
   Ataque ataqueGamma3 = new Ataque("Pu�o de titan", "final", 50, 50);
   //Ataques del caballero de Alpha
   Ataque ataqueAlpha1 = new Ataque("Poder de la estrella Alpha", "golpe", 20, 20);
   Ataque ataqueAlpha2 = new Ataque("Espada de Odin", "poder", 30, 30);
   Ataque ataqueAlpha3 = new Ataque("Ventisca de dragon", "final", 50, 50);
   
   
   //Ataques del caballero de Kraken
   Ataque ataqueKraken1 = new Ataque("Aurora boreal", "golpe", 20, 20);
   Ataque ataqueKraken2 = new Ataque("Polvo de diamantes extremo", "poder", 30, 30);
   Ataque ataqueKraken3 = new Ataque("Torbellino polar", "final", 50,50);
   //Ataques del caballero de Sirena
   Ataque ataqueSirena1 = new Ataque("Flauta dulce", "golpe", 20, 20);
   Ataque ataqueSirena2 = new Ataque("Sinfonia mortal", "poder", 30, 30);
   Ataque ataqueSirena3 = new Ataque("Climax mortal", "final", 50, 50);
   //Ataques del caballero de Chrysaor
   Ataque ataqueChrysaor1 = new Ataque("Lanzada destelleante", "golpe", 20, 20);
   Ataque ataqueChrysaor2 = new Ataque("Levitacion", "poder", 30, 30);
   Ataque ataqueChrysaor3 = new Ataque("Barrera de chakra", "final", 50, 50);
   //Ataques del caballero de Caballo Marino
   Ataque ataqueCaballoMarino1 = new Ataque("Oleadas ascendentes", "golpe", 20, 20);
   Ataque ataqueCaballoMarino2 = new Ataque("Soplo divino", "poder", 30, 30);
   Ataque ataqueCaballoMarino3 = new Ataque("Barrera protectora", "final", 50, 50);
   //Ataques del caballero de Dragon de los Mares
   Ataque ataqueDragonDeLosMares1 = new Ataque("Triangulo dorado", "golpe", 20, 20);
   Ataque ataqueDragonDeLosMares2 = new Ataque("Otra dimension", "poder", 30, 30);
   Ataque ataqueDragonDeLosMares3 = new Ataque("Explosion galactica", "final", 50, 50);
   
   
   //Vamos a asignar los ataques a los caballeros de bronce
   Ataque[]ataqueDragon={ataqueDragon1,ataqueDragon2,ataqueDragon3};
   Ataque[]ataquePegaso={ataquePegaso1,ataquePegaso2,ataquePegaso3};
   Ataque[]ataqueCisne={ataqueCisne1,ataqueCisne2,ataqueCisne3};
   Ataque[]ataqueFenix={ataqueFenix1,ataqueFenix2,ataqueFenix3};
   Ataque[]ataqueAndromeda={ataqueAndromeda1,ataqueAndromeda2,ataqueAndromeda3};
   
   //Vamos a asignar los ataques a los caballeros de oro
   Ataque[]ataqueLeo={ataqueLeo1,ataqueLeo2,ataqueLeo3};
   Ataque[]ataqueGeminis={ataqueGeminis1,ataqueGeminis2,ataqueGeminis3};
   Ataque[]ataqueTauro={ataqueTauro1,ataqueTauro2,ataqueTauro3};
   Ataque[]ataqueEscorpio={ataqueEscorpio1,ataqueEscorpio2,ataqueEscorpio3};
   Ataque[]ataqueAries={ataqueAries1,ataqueAries2,ataqueAries3};
   
   //Vamos a asignar los ataques a los caballeros de hilda
   Ataque[]ataqueBeta={ataqueBeta1,ataqueBeta2,ataqueBeta3};
   Ataque[]ataqueZeta={ataqueZeta1,ataqueZeta2,ataqueZeta3};
   Ataque[]ataqueGamma={ataqueGamma1,ataqueGamma2,ataqueGamma3};
   Ataque[]ataqueEpsilon={ataqueEpsilon1,ataqueEpsilon2,ataqueEpsilon3};
   Ataque[]ataqueAlpha={ataqueAlpha1,ataqueAlpha2,ataqueAlpha3};
   
   //Vamos a asignar los ataques a los caballeros de poseidon
   Ataque[]ataqueSirena={ataqueSirena1,ataqueSirena2,ataqueSirena3};
   Ataque[]ataqueKraken={ataqueKraken1,ataqueKraken2,ataqueKraken3};
   Ataque[]ataqueDragonDeLosMares={ataqueDragonDeLosMares1,ataqueDragonDeLosMares2,ataqueDragonDeLosMares3};
   Ataque[]ataqueCaballoMarino={ataqueCaballoMarino1,ataqueCaballoMarino2,ataqueCaballoMarino3};
   Ataque[]ataqueChrysaor={ataqueChrysaor1,ataqueChrysaor2,ataqueChrysaor3};
   
 //Vamos a crear los caballeros de Bronce
   CaballeroBronce caballeroBronceDragon = new CaballeroBronce("Dragon","Es el caballero del Dragon.\nFue entrenado por Dokho, un peque�o anciano que en realidad\n era el caballero de Libra,en las monta�as del norte de China.\n Su entrenamiento fue a trav�s del conocimiento de su cuerpo y la naturaleza.\n Posee un tatuaje en la espalda que s�lo aparece cuando su cosmos est� al maximo.\n La garra del Drag�n de su tatuaje muestra el lugar exacto de su coraz�n\n lo que le hace vulnerable.\n Su principal motivaci�n es la busqueda de los causantes de la muerte \nde su amada Shunrei.","Shiryu","bronce","China",100,100,ataqueDragon);
   CaballeroBronce caballeroBroncePegaso = new CaballeroBronce("Pegaso","Es el caballero de Pegaso. \nEs de origen asi�tico, pero fu� entrenado en Grecia,su maestra\n fue Marin (una caballero de plata que vivia en la acr�polis).\n Fue victima de burlas y constantes palizas por su origen,pero derroto a\n Cassius y pudo conseguir la armadura de Pegaso. \nSu principal motivaci�n es recuperar a su hermana Seika de la que\n fu� separado en su infancia.","Seiya","bronce","Grecia",100,100,ataquePegaso);
   CaballeroBronce caballeroBronceFenix = new CaballeroBronce("Fenix","Es el caballero del Fenix. \nFu� entrenado en Ecuador,su maestro fue Guilty. \nSu entrenamiento fue a trav�s de la dureza y la inculcaci�n del odio.\nAl final de su entrenamiento acab� con la vida de su maestro.\nSu principal motivaci�n es la de convertirse en el caballero m�s poderoso \ny as� vengarse de los que lo trataron mal en su infancia.","Ikki","bronce","Ecuador",100,100,ataqueFenix);
   CaballeroBronce caballeroBronceAndromeda = new CaballeroBronce("Andromeda","Es el caballero de Andromeda. \nFu� entrenado en Etiopia, su maestro fue Daidalos de Cefeo. \nFue entrenado en el uso de las cadenas de su constelaci�n como armas, \nasi como en el cuerpo a cuerpo. \nSu principal motivaci�n es la de volver a reunirse con su hermano Ikki, \ndel cual lo separaron a muy pronta edad.","Shun","bronce","Etiopia",100,100,ataqueAndromeda);
   CaballeroBronce caballeroBronceCisne = new CaballeroBronce("Cisne","Es el caballero del Cisne. \nFu� entrenado en Siberia,su maestro fu� Cystal Saint (a su vez discipulo de Camus de Acuario).\nSus ataques se basan en el poder de la Aurora Boreal.\nSu motivaci�n es la de conseguir el poder necesario para rescatar a \nsu madre muerta que yace en el fondo del oc�ano �rtico dentro de un barco.","Hyoga","bronce","Rusia",100,100,ataqueCisne);
   
   //Vamos a crear los caballeros de Oro
   CaballeroOro caballeroOroLeo = new CaballeroOro("Leo","Aiora","oro","Grecia",120,120,ataqueLeo);
   CaballeroOro caballeroOroGeminis = new CaballeroOro("Geminis","Saga","oro","Grecia",120,120,ataqueGeminis);
   CaballeroOro caballeroOroTauro = new CaballeroOro("Tauro","Aldebaran","oro","Brasil",120,120,ataqueTauro);
   CaballeroOro caballeroOroAries = new CaballeroOro("Aries","Mu","oro","Tibet",120,120,ataqueAries);
   CaballeroOro caballeroOroEscorpio = new CaballeroOro("Escorpio","Milo","oro","Grecia",120,120,ataqueEscorpio);
   
   //Vamos a crear los caballeros de Hilda
   CaballeroHilda caballeroHildaZeta = new CaballeroHilda("Zeta","Syd","zafiro","Noruega",150,150,ataqueZeta);
   CaballeroHilda caballeroHildaEpsilon = new CaballeroHilda("Epsilon","Fenrir","zafiro","Noruega",150,150,ataqueEpsilon);
   CaballeroHilda caballeroHildaBeta = new CaballeroHilda("Beta","Hagen","zafiro","Noruega",150,150,ataqueBeta);
   CaballeroHilda caballeroHildaGamma = new CaballeroHilda("Gamma","Thor","zafiro","Noruega",150,150,ataqueGamma);
   CaballeroHilda caballeroHildaAlpha = new CaballeroHilda("Alpha","Sigfried","zafiro","Noruega",150,150,ataqueAlpha);
    
   //Vamos a crear los caballeros de Poseidon
   CaballeroPoseidon caballeroPoseidonKraken = new CaballeroPoseidon("Kraken","Isaac","escamas","Finlandia",200,200,ataqueKraken);
   CaballeroPoseidon caballeroPoseidonSirena = new CaballeroPoseidon("Sirena","Sorrento","escamas","Austria",200,200,ataqueSirena);
   CaballeroPoseidon caballeroPoseidonChrysaor = new CaballeroPoseidon("Chrysaor","Krishna","escamas","India",200,200,ataqueChrysaor);
   CaballeroPoseidon caballeroPoseidonCaballoMarino = new CaballeroPoseidon("Caballo Marino","Baian","escamas","Canada",200,200,ataqueCaballoMarino);
   CaballeroPoseidon caballeroPoseidonDragonDeLosMares = new CaballeroPoseidon("Dragon de los Mares","Kanon","escamas","Grecia",200,200,ataqueDragonDeLosMares); 
   
 //Vamos a introducir los caballeros en un array 
   CaballeroBronce[]caballeroBronce={caballeroBronceDragon,caballeroBroncePegaso,caballeroBronceCisne,caballeroBronceAndromeda,caballeroBronceFenix};
   CaballeroOro[]caballeroOro={caballeroOroLeo,caballeroOroTauro,caballeroOroAries,caballeroOroEscorpio,caballeroOroGeminis};
   CaballeroHilda[]caballeroHilda={caballeroHildaZeta,caballeroHildaEpsilon,caballeroHildaBeta,caballeroHildaGamma,caballeroHildaAlpha};
   CaballeroPoseidon[]caballeroPoseidon={caballeroPoseidonKraken,caballeroPoseidonSirena,caballeroPoseidonChrysaor,caballeroPoseidonCaballoMarino,caballeroPoseidonDragonDeLosMares};
   	
	
	/*Para llamar a la imagen
    imagenEnemigo.setIcon(imagenEnemigoBatalla[numero]);
    botonAtacar.setIcon(new ImageIcon("./imagenes/botonEspada.png"));
   */
 //Vamos a crear los distintos escenarios en los que se van a desarrollar los combates
   Escenario torneo = new Escenario("torneo",null,caballeroBronce);
   Escenario santuario = new Escenario("santuario",null,caballeroOro);
   Escenario asgard = new Escenario("asgard",null,caballeroHilda);
   Escenario poseidon = new Escenario("poseid�n",null,caballeroPoseidon);   
   
   //Vamos a introducir las imagenes de los personajes en un array
   //Comenzamos con los rivales del Torneo Gal�ctico 
 /* ImageIcon[] imagenEnemigoTorneo = new ImageIcon[5];		
    imagenEnemigoTorneo[0] = new ImageIcon(".\\imagenes\\Shiryu2.jpg");
  	imagenEnemigoTorneo[1] = new ImageIcon(".\\imagenes\\Seiya2.jpg");
  	imagenEnemigoTorneo[2] = new ImageIcon(".\\imagenes\\Yoga2.jpg");
  	imagenEnemigoTorneo[3] = new ImageIcon(".\\imagenes\\Shun2.jpg");
  	imagenEnemigoTorneo[4] = new ImageIcon(".\\imagenes\\Ikki2.jpg");
   //Seguimos con los enemigos del Santuario.
  	ImageIcon[] imagenEnemigoSantuario = new ImageIcon[5];
  	imagenEnemigoSantuario[0] = new ImageIcon(".\\imagenes\\Leo2.jpg");
  	imagenEnemigoSantuario[1] = new ImageIcon(".\\imagenes\\tauro2.jpg");
  	imagenEnemigoSantuario[2] = new ImageIcon(".\\imagenes\\aries2.jpg");
  	imagenEnemigoSantuario[3] = new ImageIcon(".\\imagenes\\escorpio2.jpg");
  	imagenEnemigoSantuario[4] = new ImageIcon(".\\imagenes\\geminis2.jpg");
   //Los siguientes enemigos son los de Asgard
  	ImageIcon[] imagenEnemigoAsgard = new ImageIcon[5];
  	imagenEnemigoAsgard[0] = new ImageIcon(".\\imagenes\\zeta2.jpg");
  	imagenEnemigoAsgard[1] = new ImageIcon(".\\imagenes\\epsilon2.jpg");
  	imagenEnemigoAsgard[2] = new ImageIcon(".\\imagenes\\beta2.jpg");
  	imagenEnemigoAsgard[3] = new ImageIcon(".\\imagenes\\gamma2.jpg");
  	imagenEnemigoAsgard[4] = new ImageIcon(".\\imagenes\\alpha2.jpg");
   //Los siguientes enemigos son los caballeros de Poseid�n.
  	ImageIcon[] imagenEnemigoPoseidon = new ImageIcon[5];
  	imagenEnemigoPoseidon[0] = new ImageIcon(".\\imagenes\\kraken2.jpg");
  	imagenEnemigoPoseidon[1] = new ImageIcon(".\\imagenes\\sirena2.jpg");
  	imagenEnemigoPoseidon[2] = new ImageIcon(".\\imagenes\\Chrysaor2.jpg");
  	imagenEnemigoPoseidon[3] = new ImageIcon(".\\imagenes\\hipocampo2.jpg");
  	imagenEnemigoPoseidon[4] = new ImageIcon(".\\imagenes\\dragon_del_mar2.jpg");*/
  
}

